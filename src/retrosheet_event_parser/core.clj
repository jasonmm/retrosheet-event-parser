(ns retrosheet-event-parser.core
  (:require
    [clojure.java.io :as io]
    [instaparse.core :as insta]))

(defn event-instaparser
  "Create an Instaparser for `event.bnf`."
  []
  (-> "retrosheet-event/event.bnf" io/resource insta/parser))

(defn parse
  "Parses the given `event-text` and returns the raw result. Uses `parser` if
  that is given."
  ([event-text]
   (parse (event-instaparser) event-text))
  ([parser event-text]
   (insta/parse parser event-text)))
