(ns retrosheet-event-parser.core-test
  (:require
    [clojure.test :refer :all]
    [retrosheet-event-parser.core :as sut]))

(defn run-parse-test
  "A general function to run all parsing tests."
  [event-text expected-result]
  (let [parse-result (sut/parse event-text)]
    (is (= 0 (compare expected-result parse-result)))))

(deftest parsing-test
  (testing "S9/G34"
    (run-parse-test "S9/G34" [:event
                              [:description [:fielded-event "S"] [:fielder "9"]]
                              [:modifier "/" "G"]
                              [:field-location [:fielder "3"] [:fielder "4"]]
                              [:advances]]))
  (testing "64(1)/FO/G6"
    (run-parse-test "64(1)/FO/G6" [:event
                                   [:description
                                    [:fielder "6"]
                                    [:fielder "4"]
                                    [:runner-out "(" [:BAG "1"] ")"]]
                                   [:modifier "/" "FO"]
                                   [:modifier "/" "G"]
                                   [:field-location [:fielder "6"]]
                                   [:advances]]))
  (testing "64(1)3/GDP/G6"
    (run-parse-test "64(1)3/GDP/G6" [:event
                                     [:description
                                      [:fielder "6"]
                                      [:fielder "4"]
                                      [:runner-out "(" [:BAG "1"] ")"]
                                      [:fielder "3"]]
                                     [:modifier "/" "GDP"]
                                     [:modifier "/" "G"]
                                     [:field-location [:fielder "6"]]
                                     [:advances]]))
  (testing "63/G6M"
    (run-parse-test "63/G6M" [:event
                              [:description [:fielder "6"] [:fielder "3"]]
                              [:modifier "/" "G"]
                              [:field-location [:fielder "6"] [:location-modifier "M"]]
                              [:advances]]))
  (testing "DGR/L9D.2-H;1-3"
    (run-parse-test "DGR/L9D.2-H;1-3" [:event
                                       [:description [:unfielded-event "DGR"]]
                                       [:modifier "/" "L"]
                                       [:field-location [:fielder "9"] [:location-modifier "D"]]
                                       [:advances
                                        "."
                                        [:advance [:successful-advance [:BAG "2"] "-" [:HOME "H"]]]
                                        ";"
                                        [:advance [:successful-advance [:BAG "1"] "-" [:BAG "3"]]]]]))
  (testing "8/SF/F89M.3-H"
    (run-parse-test "8/SF/F89M.3-H" [:event
                                     [:description [:fielder "8"]]
                                     [:modifier "/" "SF"]
                                     [:modifier "/" "F"]
                                     [:field-location
                                      [:fielder "8"]
                                      [:fielder "9"]
                                      [:location-modifier "M"]]
                                     [:advances
                                      "."
                                      [:advance [:successful-advance [:BAG "3"] "-" [:HOME "H"]]]]]))
  (testing "7/F78D"
    (run-parse-test "7/F78D" [:event
                              [:description [:fielder "7"]]
                              [:modifier "/" "F"]
                              [:field-location
                               [:fielder "7"]
                               [:fielder "8"]
                               [:location-modifier "D"]]
                              [:advances]]))

  (testing "K/DP.1X2(26)"
    (run-parse-test "K/DP.1X2(26)" [:event
                                    [:description [:unfielded-event "K"]]
                                    [:modifier "/" "DP"]
                                    [:advances
                                     "."
                                     [:advance
                                      [:unsuccessful-advance
                                       [:BAG "1"]
                                       "X"
                                       [:BAG "2"]
                                       "("
                                       [:assists [:fielder "2"]]
                                       [:putout [:fielder "6"]]
                                       ")"]]]]))

  (testing "S7/L7LD.3-H;2-H;BX2(7E4)"
    (run-parse-test "S7/L7LD.3-H;2-H;BX2(7E4)" [:event
                                                [:description [:fielded-event "S"] [:fielder "7"]]
                                                [:modifier "/" "L"]
                                                [:field-location
                                                 [:fielder "7"]
                                                 [:location-modifier "L"]
                                                 [:location-modifier "D"]]
                                                [:advances
                                                 "."
                                                 [:advance [:successful-advance [:BAG "3"] "-" [:HOME "H"]]]
                                                 ";"
                                                 [:advance [:successful-advance [:BAG "2"] "-" [:HOME "H"]]]
                                                 ";"
                                                 [:advance
                                                  [:successful-advance
                                                   [:BATTER "B"]
                                                   "X"
                                                   [:BAG "2"]
                                                   [:advance-parameter
                                                    "("
                                                    [:advance-fielding [:assists [:fielder "7"]] "E" [:fielder "4"]]
                                                    ")"]]]]]))

  (testing "S5/G5.1-3(E5/TH)"
    (run-parse-test "S5/G5.1-3(E5/TH)" [:event
                                        [:description [:fielded-event "S"] [:fielder "5"]]
                                        [:modifier "/" "G"]
                                        [:field-location [:fielder "5"]]
                                        [:advances
                                         "."
                                         [:advance
                                          [:successful-advance
                                           [:BAG "1"]
                                           "-"
                                           [:BAG "3"]
                                           [:advance-parameter
                                            "("
                                            [:advance-fielding
                                             [:assists]
                                             "E"
                                             [:fielder "5"]
                                             [:advance-fielding-modifier "/" [:throwing-error "TH"]]]
                                            ")"]]]]]))

  (testing "S4/G34.2-H(E4/TH)(UR)(NR);1-3;B-2"
    (run-parse-test "S4/G34.2-H(E4/TH)(UR)(NR);1-3;B-2" [:event
                                                         [:description [:fielded-event "S"] [:fielder "4"]]
                                                         [:modifier "/" "G"]
                                                         [:field-location [:fielder "3"] [:fielder "4"]]
                                                         [:advances
                                                          "."
                                                          [:advance
                                                           [:successful-advance
                                                            [:BAG "2"]
                                                            "-"
                                                            [:HOME "H"]
                                                            [:advance-parameter
                                                             "("
                                                             [:advance-fielding
                                                              [:assists]
                                                              "E"
                                                              [:fielder "4"]
                                                              [:advance-fielding-modifier "/" [:throwing-error "TH"]]]
                                                             ")"]
                                                            [:advance-parameter "(" [:advance-modifier "UR"] ")"]
                                                            [:advance-parameter "(" [:advance-modifier "NR"] ")"]]]
                                                          ";"
                                                          [:advance [:successful-advance [:BAG "1"] "-" [:BAG "3"]]]
                                                          ";"
                                                          [:advance [:successful-advance [:BATTER "B"] "-" [:BAG "2"]]]]]))

  (testing "POCS2(136)"
    (run-parse-test "POCS2(136)" [:event
                                  [:description
                                   [:pickoff
                                    "POCS"
                                    [:BAG "2"]
                                    "("
                                    [:fielder "1"]
                                    [:fielder "3"]
                                    [:fielder "6"]
                                    ")"]]
                                  [:advances]])))
