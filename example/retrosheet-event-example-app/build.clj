(ns build
  (:require [clojure.tools.build.api :as b]))

(def lib 'retrosheet-event-example-app)
(def version "0.1.0")
(def class-dir "target/classes")
(def basis (delay (b/create-basis {:project "deps.edn"})))
(def uber-file (format "target/%s-%s-standalone.jar" (name lib) version))

(defn clean [_]
      (b/delete {:path "target"}))

(defn uber [_]
      (clean nil)
      (b/copy-dir {:src-dirs   ["src" "resources"]
                   :target-dir class-dir})
      (b/compile-clj {:basis      @basis
                      :ns-compile '[retrosheet-event-example-app.core]
                      :class-dir  class-dir})
      (b/uber {:class-dir class-dir
               :basis     @basis
               :uber-file uber-file
               :main      'retrosheet-event-example-app.core}))
