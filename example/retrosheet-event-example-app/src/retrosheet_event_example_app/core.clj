(ns retrosheet-event-example-app.core
  (:gen-class)
  (:require
    [retrosheet-event-parser.core :as rsevent]
    [clojure.pprint :as pp]))

(defn -main
  [& args]
  (if (> (count args) 0)
    (pp/pprint (rsevent/parse (first args)))
    (do
      (pp/pprint (rsevent/parse "D7"))
      (pp/pprint (rsevent/parse "K+CS3(25)/DP")))))
