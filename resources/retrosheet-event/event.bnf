event ::=
  description
  ( modifier | ( modifier field-location) | ( "/" field-location ) )*
  advances
  ( exceptional | uncertain )?
;

description ::=
  ( ( fielder+ ( runner-out* | error-fielder ) )+ |
    ( fielded-event fielder* ) |
    unfielded-event |
    k-w-base-running-event |
    ( 'K' fielder+ ) ) |
  pickoff |
  ( stolen-base ( ';' stolen-base )* ) |
  caught-stealing
;

fielded-event ::= 'S' | 'D' | 'T' | 'E' | 'FC' | 'FLE' | 'H' | 'HR' ;

unfielded-event ::=
  'DGR' | 'HR' | 'K'  | 'W'  | 'IW' | 'I'  |
  'HP'  | 'NP' | 'WP' | 'PB' | 'OA' | 'DI' |
  'BK'
;

k-w-base-running-event ::=
  ( ( 'K' | 'W' | 'IW' ) '+'
    ( stolen-base | caught-stealing | 'OA' |
      pickoff     | 'PB'            | 'WP' |
      error-fielder ) )
;

pickoff ::= ( 'PO' BAG '(' pickoff-fielding ')' | 'POCS' BAG '(' fielder+ ')' ) ;

pickoff-fielding ::= ( fielder+ | error-fielder ) ;

stolen-base ::= ( 'SB' ( '2' | '3' | 'H' ) ) ;

caught-stealing ::=
  ( 'CS' ( '2' | '3' | 'H' ) '(' caught-stealing-fielding ')' ) ;

caught-stealing-fielding ::= ( fielder+ | ( fielder 'E' fielder ) ) ;

runner-out ::= '(' ( BATTER | HOME | BAG ) ')' ;

error-fielder ::= ( 'E' fielder )

modifier ::=
  '/'
  ( 'AP'    | 'BP'    | 'BG'
  | 'BGDP'  | 'BINT'  | 'BL'
  | 'BOOT'  | 'BP'    | 'BPDP'
  | 'BR'    | 'C'     | 'COUB'
  | 'COUF'  | 'COUR'  | 'DP'
  | 'E'     | 'F'     | 'FDP'
  | 'FINT'  | 'FL'    | 'FO'
  | 'G'     | 'GDP'   | 'GTP'
  | 'IF'    | 'INT'   | 'IPHR'
  | 'L'     | 'LDP'   | 'LTP'
  | 'MREV'  | 'NDP'   | 'OBS'
  | 'P'     | 'PASS'  | 'R'
  | 'RINT'  | 'SF'    | 'SH'
  | 'TH'    | 'TP'    | 'UINT'
  | 'UREV'
  ) ( '+' | '-' )*
;

advances ::= ( '.' advance ( ';' advance )* )? ;

advance ::= ( successful-advance | unsuccessful-advance ) ;

successful-advance ::=
  ( ( BATTER | HOME | BAG ) ( '-' | 'X' ) ( BATTER | HOME | BAG ) )
  ( advance-parameter )*
;

unsuccessful-advance ::=
  ( ( BATTER | HOME | BAG ) 'X' ( BATTER | HOME | BAG ) )
  ( '(' assists putout ')' )*
;

successful-advance-modifier ::= ( '(' ( 'UR' | 'RBI' | 'NR' | 'NORBI' ) ')' ) ;

advance-fielding-modifier ::= '/' ( throwing-error | 'INT' ) ;

assists ::= fielder* ;

putout ::= fielder ;

advance-parameter ::= ( '(' ( advance-modifier | advance-fielding ) ')' ) ;

advance-fielding ::=
  ( ( ( assists putout ) | ( assists 'E' fielder ) )
    advance-fielding-modifier* )
;

advance-modifier ::=
  ( 'UR'    | 'RBI' | 'NR' |
    'NORBI' | 'TUR' | 'WP' |
    throwing-error) ;

throwing-error ::= ( 'TH' ( BAG | HOME )* )

exceptional ::= '!' ;

uncertain ::= ( '#' | '?' ) ;

BATTER ::= 'B' ;

HOME ::= 'H' ;

BAG ::= '1' | '2' | '3' ;

fielder ::= ( '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | 'C' ) exceptional*;

location-modifier ::= 'XD'  | 'D'   | 'M' |
                      'S'   | 'LD'  | 'L' |
                      'LS'  | 'MD'  | 'F'
     (* the following lines contains locations found in actual *)
     (* data, but not on the official list                     *)
     (* official list: http://www.retrosheet.org/location.htm  *)
                    | 'LDW' | 'DW'  | 'RM'
                    | 'RXD' | 'RD'  | 'RS'
                    | 'XDW' | 'DW+' | 'RF'
;

field-location ::= ( (fielder+) location-modifier* ) ;
