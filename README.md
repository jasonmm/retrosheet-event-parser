# Retrosheet Event Parser

Retrosheet Event Parser is a Clojure library for parsing [Retrosheet event text](http://www.retrosheet.org/eventfile.htm#5). The library uses a context-free grammar to perform the parsing.

![build status](https://gitlab.com/jasonmm/retrosheet-event-parser/badges/master/pipeline.svg)


# Usage

Add the following dependency to your _deps.edn_ file:

```
jasonmm/retrosheet-event-parser {:git/url "https://gitlab.com/jasonmm/retrosheet-event-parser.git"
                                 :git/sha "f2c7535526c8c38c6adb9f126b4505c25fc5662d"}
```

In your code's `:require` add:

```clojure
[retrosheet-event-parser.core :as rsevent]
```

And parse event text with:

```clojure
(rsevent/parse "D7")
```

See the example application included in the `example` directory for more.


# Tests

To run the tests:

```shell
clojure -X:test
```
